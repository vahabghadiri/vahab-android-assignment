# Adidas Assignment

## About App
This is a assignment for Adidas.

## App Architecture

I choosed the clean architecture for this app. because architecture should be
scalable i separated layers so i developed mulit-module clean app.
there is modules in application :
* App Module : application entry point which has Application class and has
core and presentation module on dependencies.
* Presentation Module :  represent presentaion layer include of android ui
component (fragments, views, etc) and ViewModels. it has domain and core modules
on dependencies.
* Domain Module : represent domain layer of clean architecture and center of
application logic. it is not an android library module and it only has core module
on dependencies. this module contains all data layer abstractions and usecases
that presentation layer use.
*Data Module : as clean architecture data layer it has all domain abstractions
implementation. this module contains repository pattern and data sources. also
it depends on domain and core module.
*Core module :  shares some util classes and libraries with other modules.

## App Patterns
In this application i used MVVM as architectural pattern. also repository pattern
in data layer. communication between views and viewModels are observe pattern.

## App Implementation
Application is single activity and i implemented 2 fragments for list and details.
i checked api and i've found a collection list and details. so i implemented a search
and a detail page. application will cache the user review to be offline first and i used
work manager to handle connectivity constraints.

## Dependencies and libraries
This dependencies could be found in versions.gradle file.

* Kotlin programming languge, core and ktx
* Google lifecycle extensions as
* Kotlin coroutines for Threading
* Dagger for DI
* Navigation component
* WorkManager
* Retrofit, OkHttp, okHttpLoggingInterceptorVerion, gsonConverterVersion and Gson for networking.
* Room as local database ORM
* Junit, mockitoKotlin, mockitoInline, coreTesting for unit testing.

## Unit Testing
Application has unit test for viewModels.

## To Reviewer
I ran out of time and i know i can do it more better specially in functionality.
I know that it would be better to have simple/light api call on list page and another api call to
get large details on details page. but since api call is already large on main list, i just get
reviews on detail page.
you will see kind of mappers hell in project. i know that i could rid of it but its important
to have model for each layer.
also i only wrote test for view models because of lack of time. i usually write unit test
for usecases, repositories, dataSources and some times converters.
i hope you accept these few tests.
i wanted to write gitlab.yml file to have simple pipeline and CI. but i didnt have time.
at the end.. i know that documentation is for public api's not any methods.
Thanks.




