package com.vahabghadiri.app

import androidx.work.Configuration
import androidx.work.WorkManager
import com.vahabghadiri.app.di.AppComponent
import com.vahabghadiri.app.di.DaggerAppComponent
import com.vahabghadiri.data.di.workmanager.AppWorkerFactory
import dagger.android.AndroidInjector
import dagger.android.support.DaggerApplication
import javax.inject.Inject

class AdidasApplication : DaggerApplication() {

    private lateinit var appComponent: AppComponent

    @Inject
    lateinit var workerFactory: AppWorkerFactory

    override fun applicationInjector(): AndroidInjector<out DaggerApplication> {
        val injector = DaggerAppComponent.builder()
            .applicationContext(this)
            .build()
        appComponent = injector
        return injector
    }

    override fun onCreate() {
        super.onCreate()
        configWorkManager()
    }

    private fun configWorkManager() {
        WorkManager.initialize(
            this,
            Configuration.Builder().setWorkerFactory(workerFactory).build()
        )
    }
}