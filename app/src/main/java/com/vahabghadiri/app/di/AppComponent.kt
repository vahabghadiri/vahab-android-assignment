package com.vahabghadiri.app.di

import android.content.Context
import com.vahabghadiri.app.AdidasApplication
import com.vahabghadiri.core.di.module.CoreModule
import com.vahabghadiri.data.di.module.DatabaseModule
import com.vahabghadiri.data.di.module.NetworkModule
import com.vahabghadiri.data.di.module.RemoteApiModule
import com.vahabghadiri.data.di.module.RepositoryModule
import com.vahabghadiri.data.di.module.WorkerModule
import com.vahabghadiri.data.di.module.WorkerMultiBindModule
import com.vahabghadiri.presentation.di.module.ActivityModule
import com.vahabghadiri.presentation.di.module.FragmentModule
import com.vahabghadiri.presentation.di.module.ViewModelModule
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjector
import dagger.android.support.AndroidSupportInjectionModule
import javax.inject.Singleton

@Singleton
@Component(
    modules = [
        AndroidSupportInjectionModule::class,
        ActivityModule::class,
        FragmentModule::class,
        ViewModelModule::class,
        CoreModule::class,
        NetworkModule::class,
        RemoteApiModule::class,
        RepositoryModule::class,
        WorkerModule::class,
        WorkerMultiBindModule::class,
        DatabaseModule::class
    ]
)
interface AppComponent : AndroidInjector<AdidasApplication> {

    @Component.Builder
    interface Builder {

        @BindsInstance
        fun applicationContext(applicationContext: Context): Builder
        fun build(): AppComponent
    }
}