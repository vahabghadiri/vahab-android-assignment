package com.vahabghadiri.domain.model.usecase

/**
 * this class represent needed params
 * for UseCases execution
 * @see UseCase
 */
open class UseCaseParam