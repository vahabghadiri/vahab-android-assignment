package com.vahabghadiri.domain.model.usecase

import com.vahabghadiri.domain.model.Result

/**
 * base interface which will be implemented by
 * domain module UseCases.
 * @param Params a UseCaseParam child classes to provide data for execution
 * @param Type result type domain type
 */
interface UseCase<Type, Params : UseCaseParam> {

    suspend fun execute(params: Params): Result<Type>
}