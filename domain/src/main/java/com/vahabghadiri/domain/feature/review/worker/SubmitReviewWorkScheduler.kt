package com.vahabghadiri.domain.feature.review.worker

interface SubmitReviewWorkScheduler {

    fun scheduleSubmitReview(productId: String, rate: Float, text: String)

    fun scheduleSubmitPendingReviews()
}