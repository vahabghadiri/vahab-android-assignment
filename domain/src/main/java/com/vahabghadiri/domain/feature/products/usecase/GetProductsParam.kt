package com.vahabghadiri.domain.feature.products.usecase

import com.vahabghadiri.domain.model.usecase.UseCaseParam

/**
 * UseCase param for using get product UseCase
 */
class GetProductsParam : UseCaseParam()