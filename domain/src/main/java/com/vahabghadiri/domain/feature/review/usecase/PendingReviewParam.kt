package com.vahabghadiri.domain.feature.review.usecase

import com.vahabghadiri.domain.model.usecase.UseCaseParam

/**
 * UseCase param for using get product review UseCase
 */
data class PendingReviewParam(
    val productId: String
) : UseCaseParam()