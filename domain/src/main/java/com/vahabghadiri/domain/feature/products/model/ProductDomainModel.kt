package com.vahabghadiri.domain.feature.products.model

data class ProductDomainModel(
    val id: String,
    val name: String,
    val description: String,
    val price: String,
    val currency: String,
    val imgUrl: String
)