package com.vahabghadiri.domain.feature.review.usecase

import com.vahabghadiri.domain.feature.details.model.ProductReviewDomainModel
import com.vahabghadiri.domain.feature.details.repository.ProductReviewRepository
import com.vahabghadiri.domain.model.Result
import com.vahabghadiri.domain.model.usecase.UseCase
import javax.inject.Inject

/**
 * UseCase to get pending user review
 * @see UseCase
 * @see Result
 */
class GetUserReviewUseCase @Inject constructor(
    private val productReviewRepository: ProductReviewRepository
) : UseCase<ProductReviewDomainModel, PendingReviewParam> {

    override suspend fun execute(params: PendingReviewParam): Result<ProductReviewDomainModel> {
        return try {
            val persistedReview = productReviewRepository.getPersistedReview(params.productId)
            if (persistedReview != null) {
                Result.Success(persistedReview.copy(id = SubmitReviewUseCase.USER_PEND_REVIEW_ID))
            } else {
                Result.Failure(NO_VALUE_MESSAGE)
            }
        } catch (e: Exception) {
            Result.Failure(e.message.orEmpty())
        }
    }

    companion object {

        const val NO_VALUE_MESSAGE = "No value"
    }
}