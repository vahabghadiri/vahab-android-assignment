package com.vahabghadiri.domain.feature.products.repository

import com.vahabghadiri.domain.feature.products.model.ProductDomainModel
import com.vahabghadiri.domain.model.Result

/**
 * product repository abstraction in domain layer
 * which is implemented by repository in data layer
 * app uses repository pattern
 */
interface ProductListRepository {

    suspend fun getProductsList(): Result<List<ProductDomainModel>>
}