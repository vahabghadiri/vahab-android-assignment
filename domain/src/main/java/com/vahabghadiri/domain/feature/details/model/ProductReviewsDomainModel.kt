package com.vahabghadiri.domain.feature.details.model

data class ProductReviewsDomainModel(
    val id: String,
    val reviews: List<ProductReviewDomainModel>
)