package com.vahabghadiri.domain.feature.products.usecase

import com.vahabghadiri.domain.feature.products.model.ProductDomainModel
import com.vahabghadiri.domain.feature.products.repository.ProductListRepository
import com.vahabghadiri.domain.model.Result
import com.vahabghadiri.domain.model.usecase.UseCase
import javax.inject.Inject

/**
 * UseCase to get products list
 * @see UseCase
 * @see Result
 */
class GetProductListUseCase @Inject constructor(
    private val productListRepository: ProductListRepository
) : UseCase<List<ProductDomainModel>, GetProductsParam> {

    override suspend fun execute(params: GetProductsParam): Result<List<ProductDomainModel>> {
        return productListRepository.getProductsList()
    }
}