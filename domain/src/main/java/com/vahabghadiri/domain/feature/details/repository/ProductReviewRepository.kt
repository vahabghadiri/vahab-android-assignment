package com.vahabghadiri.domain.feature.details.repository

import com.vahabghadiri.domain.feature.details.model.ProductReviewDomainModel
import com.vahabghadiri.domain.feature.details.model.ProductReviewsDomainModel
import com.vahabghadiri.domain.model.Result

/**
 * product repository abstraction in domain layer
 * which is implemented by repository in data layer
 * app uses repository pattern
 */
interface ProductReviewRepository {

    suspend fun getProductReviews(id: String): Result<ProductReviewsDomainModel>

    suspend fun submitReview(productId: String, text: String, rate: Float): Result<Unit>

    suspend fun persistReview(productId: String, rate: Float, text: String)

    suspend fun getPersistedReview(productId: String): ProductReviewDomainModel?

    suspend fun getAllPersistedReview(): List<ProductReviewDomainModel>
}