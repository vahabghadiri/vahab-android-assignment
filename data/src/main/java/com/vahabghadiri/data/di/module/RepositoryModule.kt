package com.vahabghadiri.data.di.module

import com.vahabghadiri.data.feature.review.ProductReviewRepositoryImpl
import com.vahabghadiri.data.feature.products.ProductListRepositoryImpl
import com.vahabghadiri.domain.feature.details.repository.ProductReviewRepository
import com.vahabghadiri.domain.feature.products.repository.ProductListRepository
import dagger.Binds
import dagger.Module

/**
 * this module will bind all domain layer abstractions to data
 * layer implementations.
 */
@Module
abstract class RepositoryModule {

    @Binds
    abstract fun bindProductsRepository(
        productListRepositoryImpl: ProductListRepositoryImpl
    ): ProductListRepository

    @Binds
    abstract fun bindProductReviewRepositoryImpl(
        productReviewRepositoryImpl: ProductReviewRepositoryImpl
    ): ProductReviewRepository
}