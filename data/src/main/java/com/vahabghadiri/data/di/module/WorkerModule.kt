package com.vahabghadiri.data.di.module

import com.vahabghadiri.data.feature.review.worker.SubmitReviewWorkSchedulerImpl
import com.vahabghadiri.domain.feature.review.worker.SubmitReviewWorkScheduler
import dagger.Binds
import dagger.Module

/**
 * this module will bind all domain layer abstractions to data
 * layer implementations.
 */
@Module
abstract class WorkerModule {

    @Binds
    abstract fun bindReviewWorker(
        SubmitReviewWorkSchedulerImpl: SubmitReviewWorkSchedulerImpl
    ): SubmitReviewWorkScheduler
}