package com.vahabghadiri.data.feature.review.worker

import android.content.Context
import androidx.work.Data
import androidx.work.ListenableWorker
import androidx.work.Worker
import androidx.work.WorkerParameters
import com.vahabghadiri.data.di.workmanager.ChildWorkerFactory
import com.vahabghadiri.data.feature.review.ProductReviewRepositoryImpl
import dagger.assisted.Assisted
import dagger.assisted.AssistedInject
import kotlinx.coroutines.runBlocking
import javax.inject.Inject

class SubmitReviewWorker @AssistedInject constructor(
    @Assisted context: Context,
    @Assisted workerParams: WorkerParameters,
    private val productReviewRepositoryImpl: ProductReviewRepositoryImpl
) : Worker(context, workerParams) {

    override fun doWork(): Result {
        val id = inputData.getString(PRODUCT_ID_KEY)
        val text = inputData.getString(TEXT_KEY)
        val rate = inputData.getFloat(RATE_KEY, -1F)

        if (rate == -1F || id.isNullOrBlank()) {
            return Result.failure()
        }
        val apiResult = runBlocking {
            productReviewRepositoryImpl.submitReview(
                id,
                text.orEmpty(),
                rate
            )
        }
        return if (apiResult is com.vahabghadiri.domain.model.Result.Success) {
            Result.success()
        } else {
            Result.failure()
        }
    }

    class Factory @Inject constructor(
        private val productReviewRepositoryImpl: ProductReviewRepositoryImpl
    ) : ChildWorkerFactory {

        override fun create(appContext: Context, params: WorkerParameters): ListenableWorker {
            return SubmitReviewWorker(
                appContext,
                params,
                productReviewRepositoryImpl
            )
        }
    }

    companion object {

        fun toInputData(id: String, text: String, rate: Float): Data {
            return Data.Builder()
                .putString(PRODUCT_ID_KEY, id)
                .putString(TEXT_KEY, text)
                .putFloat(RATE_KEY, rate)
                .build()
        }

        const val PRODUCT_ID_KEY = "productId"
        const val TEXT_KEY = "text"
        const val RATE_KEY = "rate"
    }
}