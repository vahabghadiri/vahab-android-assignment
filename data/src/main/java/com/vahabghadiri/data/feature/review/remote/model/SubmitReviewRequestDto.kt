package com.vahabghadiri.data.feature.review.remote.model

import com.google.gson.annotations.SerializedName

data class SubmitReviewRequestDto(
    @SerializedName("rating") val rate: Float,
    @SerializedName("text") val text: String
)