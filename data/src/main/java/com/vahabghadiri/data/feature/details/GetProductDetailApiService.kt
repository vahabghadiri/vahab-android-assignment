package com.vahabghadiri.data.feature.details

import com.vahabghadiri.data.feature.details.model.ProductDetailResponseDto
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Path

interface GetProductDetailApiService {

    @GET("product/{id}")
    fun getProductDetail(@Path("id") id: String): Call<ProductDetailResponseDto>
}