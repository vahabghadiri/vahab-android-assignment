package com.vahabghadiri.data.feature.review

import com.vahabghadiri.data.feature.review.model.ProductReviewsDataModel
import com.vahabghadiri.data.feature.review.model.ProductReviewDataModel
import com.vahabghadiri.data.feature.details.model.ProductDetailResponseDto
import com.vahabghadiri.data.feature.review.local.model.ReviewLocalEntity
import com.vahabghadiri.data.feature.review.model.SubmitReviewDataModel
import com.vahabghadiri.data.feature.review.remote.model.ProductReviewResponseDto
import com.vahabghadiri.data.feature.review.remote.model.SubmitReviewResponseDto
import com.vahabghadiri.domain.feature.details.model.ProductReviewsDomainModel
import com.vahabghadiri.domain.feature.details.model.ProductReviewDomainModel

fun ProductDetailResponseDto.toDataModel() = ProductReviewsDataModel(
    id,
    reviews.map {
        it.toDataModel()
    }
)

fun ProductReviewResponseDto.toDataModel() = ProductReviewDataModel(
    id, rate, text
)

fun ProductReviewsDataModel.toDomainModel() = ProductReviewsDomainModel(
    id,
    reviews.map {
        it.toDomainModel()
    }
)

fun ProductReviewDataModel.toDomainModel() = ProductReviewDomainModel(
    id, rate, text
)

fun SubmitReviewResponseDto.toDataModel() = SubmitReviewDataModel(
    id, text, rate
)

fun SubmitReviewDataModel.toLocalEntity() = ReviewLocalEntity(
    productId, text, rate
)

fun ReviewLocalEntity.toDataModel() = SubmitReviewDataModel(
    id, text, rate
)

fun SubmitReviewDataModel.toDomainModel() = ProductReviewDomainModel(
    productId, rate, text
)