package com.vahabghadiri.data.feature.review.local.model

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "pending_review")
data class ReviewLocalEntity(

    @PrimaryKey
    val id: String,
    val text: String,
    val rate: Float
)