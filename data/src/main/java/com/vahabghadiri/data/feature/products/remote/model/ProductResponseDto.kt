package com.vahabghadiri.data.feature.products.remote.model

import com.google.gson.annotations.SerializedName

data class ProductResponseDto(
    @SerializedName("id") val id: String,
    @SerializedName("name") val name: String,
    @SerializedName("description") val description: String,
    @SerializedName("price") val price: String,
    @SerializedName("currency") val currency: String,
    @SerializedName("imgUrl") val imgUrl: String
)