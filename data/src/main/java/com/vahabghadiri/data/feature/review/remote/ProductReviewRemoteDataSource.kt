package com.vahabghadiri.data.feature.review.remote

import com.vahabghadiri.core.utils.GlobalDispatcher
import com.vahabghadiri.data.feature.details.GetProductDetailApiService
import com.vahabghadiri.data.feature.review.model.ProductReviewsDataModel
import com.vahabghadiri.data.feature.review.model.SubmitReviewDataModel
import com.vahabghadiri.data.feature.review.remote.model.SubmitReviewRequestDto
import com.vahabghadiri.data.feature.review.remote.model.SubmitReviewResponseDto
import com.vahabghadiri.data.feature.review.toDataModel
import com.vahabghadiri.data.util.ApiResult
import com.vahabghadiri.data.util.callAwait
import kotlinx.coroutines.withContext
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class ProductReviewRemoteDataSource @Inject constructor(
    private val getProductDetailApiService: GetProductDetailApiService,
    private val productReviewApiService: ProductReviewApiService,
    private val globalDispatcher: GlobalDispatcher
) {

    /**
     * It is better to have thread select here because each data provider knows
     * better that which thread is suitable for this data
     * and also it prevent issues in other layers.
     */
    suspend fun getProductReviews(id: String): ApiResult<ProductReviewsDataModel> {
        return withContext(globalDispatcher.io) {
            getProductDetailApiService.getProductDetail(id).callAwait {
                it.toDataModel()
            }
        }
    }

    suspend fun submitReview(
        productId: String,
        text: String,
        rate: Float
    ): ApiResult<SubmitReviewDataModel> {
        val request = SubmitReviewRequestDto(rate, text)
        return withContext(globalDispatcher.io) {
            productReviewApiService.submitReview(productId, request).callAwait {
                it.toDataModel()
            }
        }
    }
}