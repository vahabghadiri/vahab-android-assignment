package com.vahabghadiri.data.feature.products

import com.vahabghadiri.data.feature.products.remote.ProductsListRemoteDataSource
import com.vahabghadiri.data.util.ApiResult
import com.vahabghadiri.domain.feature.products.model.ProductDomainModel
import com.vahabghadiri.domain.feature.products.repository.ProductListRepository
import com.vahabghadiri.domain.model.Result
import javax.inject.Inject
import javax.inject.Singleton

/**
 * implementation for getting products list repository abstraction in
 * domain layer.
 * @see ProductListRepository
 */
@Singleton
class ProductListRepositoryImpl @Inject constructor(
    private val remoteDataSource: ProductsListRemoteDataSource
) : ProductListRepository {

    override suspend fun getProductsList(): Result<List<ProductDomainModel>> {
        return when (val apiResult = remoteDataSource.getProducts()) {
            is ApiResult.Success -> {
                val domainObject = apiResult.value.map {
                    it.toDomainModel()
                }
                Result.Success(domainObject)
            }
            is ApiResult.Failure -> {
                Result.Failure(apiResult.error.message ?: UNKNOWN_API_EXCEPTION)
            }
        }
    }

    companion object {

        private const val UNKNOWN_API_EXCEPTION = "unknown api exception"
    }
}