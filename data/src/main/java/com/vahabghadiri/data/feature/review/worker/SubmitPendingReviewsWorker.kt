package com.vahabghadiri.data.feature.review.worker

import android.content.Context
import androidx.work.Data
import androidx.work.ListenableWorker
import androidx.work.Worker
import androidx.work.WorkerParameters
import com.vahabghadiri.data.di.workmanager.ChildWorkerFactory
import com.vahabghadiri.data.feature.review.ProductReviewRepositoryImpl
import com.vahabghadiri.domain.model.Result
import dagger.assisted.Assisted
import dagger.assisted.AssistedInject
import kotlinx.coroutines.runBlocking
import javax.inject.Inject

class SubmitPendingReviewsWorker @AssistedInject constructor(
    @Assisted context: Context,
    @Assisted workerParams: WorkerParameters,
    private val productReviewRepositoryImpl: ProductReviewRepositoryImpl
) : Worker(context, workerParams) {

    override fun doWork(): Result {

        val batchReviewResult = runBlocking {
            val pendingReviews = productReviewRepositoryImpl.getAllPersistedReview()

            // it was better to have batch review api
            var aggregateResult = true
            pendingReviews.asReversed()
                .forEach { review ->
                    val apiResult = productReviewRepositoryImpl.submitReview(
                        review.id, review.text, review.rate
                    )
                    // with this line we AND failed result
                    aggregateResult = apiResult is com.vahabghadiri.domain.model.Result.Success &&
                            aggregateResult
                }
            aggregateResult
        }
        return if (batchReviewResult) {
            Result.success()
        } else {
            Result.failure()
        }
    }

    class Factory @Inject constructor(
        private val productReviewRepositoryImpl: ProductReviewRepositoryImpl
    ) : ChildWorkerFactory {

        override fun create(appContext: Context, params: WorkerParameters): ListenableWorker {
            return SubmitPendingReviewsWorker(
                appContext,
                params,
                productReviewRepositoryImpl
            )
        }
    }
}