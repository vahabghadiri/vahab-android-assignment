package com.vahabghadiri.data.feature.products.model

data class ProductDataModel(
    val id: String,
    val name: String,
    val description: String,
    val price: String,
    val currency: String,
    val imgUrl: String
)