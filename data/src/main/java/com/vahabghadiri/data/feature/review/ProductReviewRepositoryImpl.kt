package com.vahabghadiri.data.feature.review

import com.vahabghadiri.data.feature.review.local.ProductReviewLocalDataSource
import com.vahabghadiri.data.feature.review.model.SubmitReviewDataModel
import com.vahabghadiri.data.feature.review.remote.ProductReviewRemoteDataSource
import com.vahabghadiri.data.util.ApiResult
import com.vahabghadiri.domain.feature.details.model.ProductReviewDomainModel
import com.vahabghadiri.domain.feature.details.model.ProductReviewsDomainModel
import com.vahabghadiri.domain.feature.details.repository.ProductReviewRepository
import com.vahabghadiri.domain.model.Result
import javax.inject.Inject
import javax.inject.Singleton

/**
 * implementation for getting product detail repository abstraction in
 * domain layer.
 * @see ProductReviewRepository
 */
@Singleton
class ProductReviewRepositoryImpl @Inject constructor(
    private val remoteDataSource: ProductReviewRemoteDataSource,
    private val localDataSource: ProductReviewLocalDataSource
) : ProductReviewRepository {

    override suspend fun getProductReviews(id: String): Result<ProductReviewsDomainModel> {
        return when (val apiResult = remoteDataSource.getProductReviews(id)) {
            is ApiResult.Success -> {
                Result.Success(apiResult.value.toDomainModel())
            }
            is ApiResult.Failure -> {
                Result.Failure(apiResult.error.message ?: UNKNOWN_API_EXCEPTION)
            }
        }
    }

    override suspend fun submitReview(
        productId: String,
        text: String,
        rate: Float
    ): Result<Unit> {
        return when (val apiResult = remoteDataSource.submitReview(productId, text, rate)) {
            is ApiResult.Success -> {
                removePersistedReview(productId)
                Result.Success(Unit)
            }
            is ApiResult.Failure -> {
                Result.Failure(apiResult.error.message ?: UNKNOWN_API_EXCEPTION)
            }
        }
    }

    /**
     * will store review as a pending record
     */
    override suspend fun persistReview(productId: String, rate: Float, text: String) {
        localDataSource.persistReview(SubmitReviewDataModel(productId, text, rate))
    }

    @Throws
    override suspend fun getPersistedReview(productId: String): ProductReviewDomainModel? {
        return localDataSource.getPersistedReview(productId)?.toDomainModel()
    }

    @Throws
    override suspend fun getAllPersistedReview(): List<ProductReviewDomainModel> {
        return localDataSource.getAllPersistedReviews().map {
            it.toDomainModel()
        }
    }

    private suspend fun removePersistedReview(productId: String) {
        runCatching {
            localDataSource.removePersistedReview(productId)
        }
    }

    companion object {

        private const val UNKNOWN_API_EXCEPTION = "unknown api exception"
    }
}