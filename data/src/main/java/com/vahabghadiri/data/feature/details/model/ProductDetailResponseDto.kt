package com.vahabghadiri.data.feature.details.model

import com.google.gson.annotations.SerializedName
import com.vahabghadiri.data.feature.review.remote.model.ProductReviewResponseDto

data class ProductDetailResponseDto(
    @SerializedName("id") val id: String,
    @SerializedName("reviews") val reviews: List<ProductReviewResponseDto>
)