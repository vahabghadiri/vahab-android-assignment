package com.vahabghadiri.data.feature.review.remote.model

import com.google.gson.annotations.SerializedName

data class SubmitReviewResponseDto(
    @SerializedName("productId") val id: String,
    @SerializedName("text") val text: String,
    @SerializedName("rating") val rate: Float
)