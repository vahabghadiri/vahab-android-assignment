package com.vahabghadiri.data.feature.review.remote.model

import com.google.gson.annotations.SerializedName

data class ProductReviewResponseDto(
    @SerializedName("productId") val id: String,
    @SerializedName("rating") val rate: Float,
    @SerializedName("text") val text: String
)