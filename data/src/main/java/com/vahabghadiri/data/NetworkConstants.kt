package com.vahabghadiri.data

/**
 *  constants for network, we can use gradle fields
 *  instead when we want to have different urls for production
 *  and development
 */
object NetworkConstants {

    // localhost in emulator means 10.0.2.2
    const val BASE_URL = "http://10.0.2.2:3001/"

    // for reviews we need another url
    const val BASE_URL_REVIEWS = "http://10.0.2.2:3002/"
}