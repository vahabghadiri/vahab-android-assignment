package com.vahabghadiri.presentation.utils

import android.widget.ImageView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import com.bumptech.glide.request.RequestOptions
import com.vahabghadiri.presentation.R

object ImageLoader {

    fun loadThumbnail(view: ImageView, url: String?) {
        val requestOptions = RequestOptions().diskCacheStrategy(DiskCacheStrategy.ALL)
        Glide.with(view.context)
            .load(url)
            .placeholder(R.color.dark)
            .apply(requestOptions)
            .transition(DrawableTransitionOptions.withCrossFade())
            .thumbnail()
            .into(view)
    }

    fun load(view: ImageView, url: String?) {
        val requestOptions = RequestOptions().diskCacheStrategy(DiskCacheStrategy.ALL)
        Glide.with(view.context)
            .load(url)
            .placeholder(R.color.dark)
            .apply(requestOptions)
            .transition(DrawableTransitionOptions.withCrossFade())
            .into(view)
    }
}