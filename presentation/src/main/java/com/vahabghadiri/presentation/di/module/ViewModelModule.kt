package com.vahabghadiri.presentation.di.module

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.vahabghadiri.presentation.di.DaggerViewModelFactory
import com.vahabghadiri.presentation.di.ViewModelKey
import com.vahabghadiri.presentation.feature.detail.DetailsViewModel
import com.vahabghadiri.presentation.feature.products.ProductsViewModel
import com.vahabghadiri.presentation.feature.splash.SplashViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap
import javax.inject.Singleton

/**
 * di module to provide view models
 * and view model factory itseld
 */
@Module
abstract class ViewModelModule {

    @Binds
    @Singleton
    internal abstract fun bindViewModelFactory(
        factory: DaggerViewModelFactory
    ): ViewModelProvider.Factory

    @Binds
    @IntoMap
    @ViewModelKey(SplashViewModel::class)
    abstract fun bindSplashViewModel(
        splashViewModel: SplashViewModel
    ): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(ProductsViewModel::class)
    abstract fun bindProductsViewModel(
        productsViewModel: ProductsViewModel
    ): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(DetailsViewModel::class)
    abstract fun bindDetailsViewModel(
        detailsViewModel: DetailsViewModel
    ): ViewModel
}