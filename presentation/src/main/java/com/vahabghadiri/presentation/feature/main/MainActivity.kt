package com.vahabghadiri.presentation.feature.main

import android.content.Context
import android.content.Intent
import android.os.Bundle
import com.vahabghadiri.presentation.R
import com.vahabghadiri.presentation.feature.base.BaseActivity

class MainActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        overridePendingTransition(R.anim.fade_in, R.anim.fade_out)
    }

    companion object {

        fun getLaunchIntent(context: Context): Intent {
            return Intent(context, MainActivity::class.java)
        }
    }
}