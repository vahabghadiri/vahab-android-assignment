package com.vahabghadiri.presentation.feature.products

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.navigation.fragment.FragmentNavigatorExtras
import androidx.navigation.fragment.NavHostFragment.findNavController
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.snackbar.Snackbar
import com.vahabghadiri.presentation.R
import com.vahabghadiri.presentation.feature.base.BaseFragment
import com.vahabghadiri.presentation.databinding.FragmentProductsListBinding
import com.vahabghadiri.presentation.feature.products.adapter.AdapterCallback
import com.vahabghadiri.presentation.feature.products.adapter.ProductListRvAdapter
import com.vahabghadiri.presentation.feature.products.model.ProductListItem
import com.vahabghadiri.presentation.utils.extensions.createViewModel
import com.vahabghadiri.presentation.utils.extensions.observeNullSafe
import com.vahabghadiri.presentation.utils.extensions.onAfterTextChanged
import com.vahabghadiri.presentation.utils.extensions.setVisible

class ProductsFragment : BaseFragment(), AdapterCallback {

    private lateinit var viewBinding: FragmentProductsListBinding
    private lateinit var viewModel: ProductsViewModel

    private var recyclerAdapter: ProductListRvAdapter? = null
    private var failureSnackBarView: Snackbar? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        viewBinding = FragmentProductsListBinding.inflate(inflater)
        return viewBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupViews()
        setupViewModel()
        viewModel.onProductListViewCreated()
    }

    private fun setupViews() {
        // init the recyclerView
        with(viewBinding.rvList) {
            recyclerAdapter = ProductListRvAdapter(
                mutableListOf(),
                this@ProductsFragment
            )

            // i do not have time to implement landscape support, span count should change then.
            this.layoutManager = GridLayoutManager(requireContext(), 2)
            this.adapter = recyclerAdapter

            // for shared element transition
            postponeEnterTransition()
            viewTreeObserver.addOnPreDrawListener {
                startPostponedEnterTransition()
                true
            }
        }

        // init searchView
        viewBinding.searchBox.edtSearch.onAfterTextChanged {
            viewModel.onUserChangedSearchString(it)
        }
    }

    private fun setupViewModel() {
        viewModel = createViewModel(viewModelFactory) {
            updateListLiveData.observeNullSafe(viewLifecycleOwner) {
                onListNeedsToUpdate(it)
            }
            progressVisibilityLiveData.observeNullSafe(viewLifecycleOwner) { isVisible ->
                onProgressVisibilityChange(isVisible)
            }
            emptyViewVisibilityLiveData.observeNullSafe(viewLifecycleOwner) { isVisible ->
                onEmptyViewVisibilityChange(isVisible)
            }
            failureEventLiveData.observe(viewLifecycleOwner) { failureMessage ->
                handleFailureViews(failureMessage)
            }
            navigateToDetailsLiveData.observeNullSafe(viewLifecycleOwner) { (item, index) ->
                navigateToDetails(item, index)
            }
        }
    }

    private fun onListNeedsToUpdate(newList: List<ProductListItem>) {
        recyclerAdapter?.updateData(newList)
    }

    private fun handleFailureViews(failureMessage: String?) {

        fun showErrorSnackBar(message: String) {
            failureSnackBarView?.dismiss()
            failureSnackBarView = Snackbar.make(
                viewBinding.root,
                message,
                Snackbar.LENGTH_INDEFINITE
            ).apply {
                setAction(getString(R.string.retry)) {
                    viewModel.onProductListRetryClick()
                }
            }
            failureSnackBarView?.show()
        }
        if (failureMessage.isNullOrBlank()) {
            failureSnackBarView?.dismiss()
        } else {
            showErrorSnackBar(failureMessage)
        }
    }

    private fun onEmptyViewVisibilityChange(isVisible: Boolean) {
        viewBinding.emptyLayout.tvEmpty.setVisible(isVisible)
    }

    private fun onProgressVisibilityChange(isVisible: Boolean) {
        viewBinding.pgFullScreen.setVisible(isVisible)
    }

    override fun onItemClick(adapterPosition: Int) {
        recyclerAdapter?.getItemWithPosition(adapterPosition)?.let {
            viewModel.onProductClicked(it, adapterPosition)
        }
    }

    private fun navigateToDetails(product: ProductListItem, index: Int) {
        val direction = ProductsFragmentDirections.actionProductsFragmentToDetailsFragment(product)
        val transitionView = getSharedTransitionView(index)
        if (transitionView != null) {
            val extras = FragmentNavigatorExtras(
                transitionView to product.id
            )
            findNavController(this).navigate(direction, extras)
        } else {
            findNavController(this).navigate(direction)
        }
    }

    private fun getSharedTransitionView(index: Int): ImageView? {
        val listView = viewBinding.rvList.layoutManager?.findViewByPosition(index)
        return listView?.findViewById(R.id.imgProduct)
    }

    override fun onDestroy() {
        super.onDestroy()
        recyclerAdapter = null
    }
}