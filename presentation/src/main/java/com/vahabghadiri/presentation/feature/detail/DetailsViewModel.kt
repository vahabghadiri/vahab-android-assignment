package com.vahabghadiri.presentation.feature.detail

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.vahabghadiri.core.utils.GlobalDispatcher
import com.vahabghadiri.domain.feature.details.model.ProductReviewDomainModel
import com.vahabghadiri.domain.feature.details.model.ProductReviewsDomainModel
import com.vahabghadiri.domain.feature.details.usecase.GetProductReviewsParam
import com.vahabghadiri.domain.feature.details.usecase.GetProductReviewsUseCase
import com.vahabghadiri.domain.feature.review.usecase.GetUserReviewUseCase
import com.vahabghadiri.domain.feature.review.usecase.PendingReviewParam
import com.vahabghadiri.domain.feature.review.usecase.SubmitReviewParam
import com.vahabghadiri.domain.feature.review.usecase.SubmitReviewUseCase
import com.vahabghadiri.domain.model.Result
import com.vahabghadiri.presentation.feature.base.BaseViewModel
import com.vahabghadiri.presentation.feature.detail.model.ProductInfoModel
import com.vahabghadiri.presentation.feature.detail.model.ProductReviewListItem
import com.vahabghadiri.presentation.feature.products.model.ProductListItem
import com.vahabghadiri.presentation.utils.SingleLiveEvent
import kotlinx.coroutines.launch
import javax.inject.Inject

class DetailsViewModel @Inject constructor(
    private val detailsUseCase: GetProductReviewsUseCase,
    private val submitReviewUseCase: SubmitReviewUseCase,
    private val getUserReviewUseCase: GetUserReviewUseCase,
    private val globalDispatcher: GlobalDispatcher
) : BaseViewModel() {

    private val _updateInfoModelLiveData = MutableLiveData<ProductInfoModel>()
    val updateInfoModelLiveData: LiveData<ProductInfoModel> = _updateInfoModelLiveData

    private val _updateReviewsLiveData = MutableLiveData<List<ProductReviewListItem>>()
    val updateReviewsLiveData: LiveData<List<ProductReviewListItem>> = _updateReviewsLiveData

    private val _progressVisibilityLiveData = MutableLiveData<Boolean>()
    val progressVisibilityLiveData: LiveData<Boolean> = _progressVisibilityLiveData

    private val _emptyViewVisibilityLiveData = MutableLiveData<Boolean>()
    val emptyViewVisibilityLiveData: LiveData<Boolean> = _emptyViewVisibilityLiveData

    private val _failureEventLiveData = SingleLiveEvent<String?>()
    val failureEventLiveData: LiveData<String?> = _failureEventLiveData

    fun onViewCreated(product: ProductListItem) {
        updateProductInfo(product)
        getProductDetails(product.id)
    }

    private fun getProductDetails(id: String) {
        viewModelScope.launch(globalDispatcher.main) {

            val pending = getUserReviewUseCase.execute(PendingReviewParam(id))
            handlePendingValue(pending)

            setPageLoading(true)
            when (
                val result = detailsUseCase.execute(GetProductReviewsParam(id))
            ) {
                is Result.Success -> handleDataSucceed(result.value)
                is Result.Failure -> handleDataFailure(result.error)
            }
            setPageLoading(false)
        }
    }

    private fun handlePendingValue(pendingItem: Result<ProductReviewDomainModel>) {
        if (pendingItem is Result.Success) {
            _updateReviewsLiveData.value = listOf(pendingItem.value.toListItem())
        }
        handleEmptyView()
    }

    private fun handleDataFailure(error: String) {
        _failureEventLiveData.value = error
    }

    private fun handleDataSucceed(value: ProductReviewsDomainModel) {
        val reviews = value.reviews.map {
            it.toListItem()
        }
        // handle adding api results after pending item
        val currentValues = _updateReviewsLiveData.value?.toMutableList()
        val result = currentValues?.apply {
            addAll(reviews)
        } ?: reviews

        _updateReviewsLiveData.value = result
        handleEmptyView()
    }

    private fun handleEmptyView() {
        val currentValues = _updateReviewsLiveData.value ?: return
        _emptyViewVisibilityLiveData.value = currentValues.isEmpty()
    }

    private fun setPageLoading(isLoading: Boolean) {
        _progressVisibilityLiveData.value = isLoading
    }

    private fun updateProductInfo(product: ProductListItem) {
        val infoModel = ProductInfoModel(
            product.id,
            product.name,
            product.description,
            product.price,
            product.image
        )
        _updateInfoModelLiveData.value = infoModel
    }

    fun onReviewsRetryClick(id: String) {
        getProductDetails(id)
    }

    fun onUserReviewAdded(rate: Float, text: String, productId: String) {
        viewModelScope.launch(globalDispatcher.main) {
            val param = SubmitReviewParam(productId, text, rate)
            submitReviewUseCase.execute(param)

            // we can either get reviews again (using it's useCase) or adding this one to list
            updateUiWithNewReview(text, rate)
        }
    }

    private fun updateUiWithNewReview(text: String, rate: Float) {
        val currentValues = _updateReviewsLiveData.value?.toMutableList() ?: mutableListOf()

        val item = ProductReviewListItem(SubmitReviewUseCase.USER_PEND_REVIEW_ID, rate, text)
        _updateReviewsLiveData.value = currentValues.apply {
            val index = indexOfFirst { it.id == SubmitReviewUseCase.USER_PEND_REVIEW_ID }
            if (index > -1) {
                currentValues[index] = item
            } else {
                add(0, item)
            }
        }
        handleEmptyView()
    }
}