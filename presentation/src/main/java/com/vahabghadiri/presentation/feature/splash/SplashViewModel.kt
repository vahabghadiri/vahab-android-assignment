package com.vahabghadiri.presentation.feature.splash

import androidx.lifecycle.LiveData
import androidx.lifecycle.viewModelScope
import com.vahabghadiri.core.utils.GlobalDispatcher
import com.vahabghadiri.domain.feature.review.usecase.SubmitPendingReviewUseCase
import com.vahabghadiri.domain.model.usecase.UseCaseParam
import com.vahabghadiri.presentation.feature.base.BaseViewModel
import com.vahabghadiri.presentation.utils.SingleLiveEvent
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import javax.inject.Inject

class SplashViewModel @Inject constructor(
    private val submitPendingReviewUseCase: SubmitPendingReviewUseCase,
    private val globalDispatcher: GlobalDispatcher
) : BaseViewModel() {

    private val _navigateToMainLiveData = SingleLiveEvent<Unit>()
    val navigateToMainLiveData: LiveData<Unit> = _navigateToMainLiveData

    /**
     * will be called whenever splash activity
     * is created after cold/hot app loading!
     * makes some delay and fires navigation to main page
     */
    fun onSplashViewCreated() {
        viewModelScope.launch(globalDispatcher.io) {
            delay(SPLASH_DELAY_MS)
            withContext(globalDispatcher.main) {
                submitPendingReviewUseCase.execute(UseCaseParam())
                _navigateToMainLiveData.call()
            }
        }
    }

    private companion object {

        const val SPLASH_DELAY_MS = 2000L
    }
}