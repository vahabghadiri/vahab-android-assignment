package com.vahabghadiri.presentation.feature.review

interface AddReviewCallBack {

    fun onUserReviewAdd(rate: Float, text: String)
}