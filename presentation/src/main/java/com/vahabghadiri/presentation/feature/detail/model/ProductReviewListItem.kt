package com.vahabghadiri.presentation.feature.detail.model

data class ProductReviewListItem(
    val id: String,
    val rate: Float,
    val text: String
)