package com.vahabghadiri.presentation.feature.products.adapter

import androidx.recyclerview.widget.DiffUtil
import com.vahabghadiri.presentation.feature.products.model.ProductListItem

/**
 * diff util callback for search
 * it will uses name as unique id
 */
class SearchResultDiffCallback(
    private val old: List<ProductListItem>,
    private val new: List<ProductListItem>
) : DiffUtil.Callback() {

    override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        return old[oldItemPosition].id == new[newItemPosition].id
    }

    override fun getOldListSize(): Int = old.size

    override fun getNewListSize(): Int = new.size

    override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        return old[oldItemPosition].name == new[newItemPosition].name &&
                old[oldItemPosition].description == new[newItemPosition].description &&
                old[oldItemPosition].price == new[newItemPosition].price &&
                old[oldItemPosition].image == new[newItemPosition].image
    }
}