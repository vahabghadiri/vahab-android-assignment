package com.vahabghadiri.presentation.feature.products.adapter

interface AdapterCallback {
    fun onItemClick(adapterPosition: Int)
}