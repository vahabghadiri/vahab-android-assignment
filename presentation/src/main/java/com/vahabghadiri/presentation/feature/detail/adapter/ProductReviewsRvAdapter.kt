package com.vahabghadiri.presentation.feature.detail.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.vahabghadiri.presentation.R
import com.vahabghadiri.presentation.feature.detail.model.ProductReviewListItem
import com.vahabghadiri.presentation.feature.products.model.ProductListItem

/**
 * recycler view adapter for product reviews recycle view
 * this uses DiffUtil to handle changes
 * because we have local search functionality.
 */
class ProductReviewsRvAdapter(
    private val data: MutableList<ProductReviewListItem>
) : RecyclerView.Adapter<ProductReviewViewHolder>() {

    fun updateData(newList: List<ProductReviewListItem>) {
        val diffCallback = ReviewsDiffCallback(data, newList)
        val diffResult = DiffUtil.calculateDiff(diffCallback)
        data.clear()
        data.addAll(newList)
        diffResult.dispatchUpdatesTo(this)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ProductReviewViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val view = inflater.inflate(R.layout.item_list_review, parent, false)
        return ProductReviewViewHolder(view)
    }

    override fun getItemCount() = data.size

    override fun onBindViewHolder(holder: ProductReviewViewHolder, position: Int) {
        if (position != RecyclerView.NO_POSITION) {
            holder.bind(data[position])
        }
    }
}