package com.vahabghadiri.presentation.feature.detail.adapter

import androidx.recyclerview.widget.DiffUtil
import com.vahabghadiri.presentation.feature.detail.model.ProductReviewListItem

/**
 * diff util callback for reviews
 * it will uses name as unique id
 */
class ReviewsDiffCallback(
    private val old: List<ProductReviewListItem>,
    private val new: List<ProductReviewListItem>
) : DiffUtil.Callback() {

    override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        return old[oldItemPosition].id == new[newItemPosition].id
    }

    override fun getOldListSize(): Int = old.size

    override fun getNewListSize(): Int = new.size

    override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        return old[oldItemPosition].rate == new[newItemPosition].rate &&
                return old[oldItemPosition].text == new[newItemPosition].text
    }
}