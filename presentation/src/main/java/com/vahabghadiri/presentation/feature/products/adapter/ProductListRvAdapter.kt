package com.vahabghadiri.presentation.feature.products.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.vahabghadiri.presentation.R
import com.vahabghadiri.presentation.feature.products.model.ProductListItem

/**
 * recycler view adapter for products recycle view
 * this uses DiffUtil to handle changes
 * because we have local search functionality.
 */
class ProductListRvAdapter(
    private val data: MutableList<ProductListItem>,
    private val adapterCallback: AdapterCallback
) : RecyclerView.Adapter<ProductViewHolder>() {

    fun updateData(newList: List<ProductListItem>) {
        val diffCallback = SearchResultDiffCallback(data, newList)
        val diffResult = DiffUtil.calculateDiff(diffCallback)
        data.clear()
        data.addAll(newList)
        diffResult.dispatchUpdatesTo(this)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ProductViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val view = inflater.inflate(R.layout.item_list_product, parent, false)
        return ProductViewHolder(view, adapterCallback)
    }

    override fun getItemCount() = data.size

    override fun onBindViewHolder(holder: ProductViewHolder, position: Int) {
        if (position != RecyclerView.NO_POSITION) {
            holder.bind(data[position])
        }
    }

    /**
     * will get certain item in adapter with position
     */
    fun getItemWithPosition(adapterPosition: Int): ProductListItem? {
        return if (adapterPosition != RecyclerView.NO_POSITION) {
            data.getOrNull(adapterPosition)
        } else {
            null
        }
    }
}