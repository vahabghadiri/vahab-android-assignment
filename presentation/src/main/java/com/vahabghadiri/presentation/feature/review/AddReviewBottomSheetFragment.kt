package com.vahabghadiri.presentation.feature.review

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.vahabghadiri.presentation.databinding.DialogAddReviewBinding

class AddReviewBottomSheetFragment : BottomSheetDialogFragment() {

    private lateinit var viewBinding: DialogAddReviewBinding
    private var callBack: AddReviewCallBack? = null

    override fun onAttach(context: Context) {
        super.onAttach(context)
        callBack = (parentFragment ?: context) as? AddReviewCallBack?
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        viewBinding = DialogAddReviewBinding.inflate(inflater)
        return viewBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupViews()
    }

    private fun setupViews() {
        viewBinding.btnSendReview.setOnClickListener {
            onSendClick()
        }
    }

    private fun onSendClick() {
        val currentRate = viewBinding.rtBar.rating
        val currentText = viewBinding.edtText.text.toString()
        callBack?.onUserReviewAdd(currentRate, currentText)
        dismissAllowingStateLoss()
    }

    override fun onDetach() {
        callBack = null
        super.onDetach()
    }

    companion object {

        const val TAG = "addReviewFragmentTag"
    }
}