package com.vahabghadiri.presentation.feature.products.adapter

import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.vahabghadiri.presentation.R
import com.vahabghadiri.presentation.feature.products.model.ProductListItem
import com.vahabghadiri.presentation.utils.ImageLoader

/**
 * view holder for products search result
 * @see ProductListRvAdapter
 */
class ProductViewHolder(
    itemView: View,
    adapterCallback: AdapterCallback
) : RecyclerView.ViewHolder(itemView) {

    private val image = itemView.findViewById<ImageView>(R.id.imgProduct)
    private val name = itemView.findViewById<TextView>(R.id.tvProductName)
    private val description = itemView.findViewById<TextView>(R.id.tvProductDescription)
    private val price = itemView.findViewById<TextView>(R.id.tvProductPrice)

    init {
        itemView.setOnClickListener {
            if (adapterPosition != RecyclerView.NO_POSITION) {
                adapterCallback.onItemClick(adapterPosition)
            }
        }
    }

    fun bind(item: ProductListItem) {
        name.text = item.name
        description.text = item.description
        price.text = item.price
        ImageLoader.loadThumbnail(image, item.image)
        image.transitionName = item.id
    }
}