package com.vahabghadiri.presentation.feature.detail.model

data class ProductInfoModel(
    val id: String,
    val name: String,
    val description: String,
    val price: String,
    val image: String
)