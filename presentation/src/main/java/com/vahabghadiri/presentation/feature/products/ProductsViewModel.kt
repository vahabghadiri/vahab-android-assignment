package com.vahabghadiri.presentation.feature.products

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.vahabghadiri.core.utils.GlobalDispatcher
import com.vahabghadiri.domain.feature.products.model.ProductDomainModel
import com.vahabghadiri.domain.feature.products.usecase.GetProductListUseCase
import com.vahabghadiri.domain.feature.products.usecase.GetProductsParam
import com.vahabghadiri.domain.model.Result
import com.vahabghadiri.presentation.feature.base.BaseViewModel
import com.vahabghadiri.presentation.feature.products.model.ProductListItem
import com.vahabghadiri.presentation.utils.SingleLiveEvent
import kotlinx.coroutines.launch
import javax.inject.Inject

class ProductsViewModel @Inject constructor(
    private val useCase: GetProductListUseCase,
    private val globalDispatcher: GlobalDispatcher
) : BaseViewModel() {

    private val _updateListLiveData = MutableLiveData<List<ProductListItem>>()
    val updateListLiveData: LiveData<List<ProductListItem>> = _updateListLiveData

    private val _progressVisibilityLiveData = MutableLiveData<Boolean>()
    val progressVisibilityLiveData: LiveData<Boolean> = _progressVisibilityLiveData

    private val _emptyViewVisibilityLiveData = MutableLiveData<Boolean>()
    val emptyViewVisibilityLiveData: LiveData<Boolean> = _emptyViewVisibilityLiveData

    private val _failureEventLiveData = SingleLiveEvent<String?>()
    val failureEventLiveData: LiveData<String?> = _failureEventLiveData

    private val _navigateToDetailsLiveData = SingleLiveEvent<Pair<ProductListItem, Int>>()
    val navigateToDetailsLiveData: LiveData<Pair<ProductListItem, Int>> = _navigateToDetailsLiveData

    /**
     * to handle local search view model store latest items
     * and will clear it on certain situations
     */
    private var latestItems: MutableList<ProductListItem> = mutableListOf()

    fun onProductListViewCreated() {
        if (latestItems.isNotEmpty()) {
            return
        }
        getProductList()
    }

    /**
     * will get products list from repository through useCase
     * in main thread since repository has its own data flow execution
     * decision
     */
    private fun getProductList() {
        viewModelScope.launch(globalDispatcher.main) {
            setPageLoading(true)
            when (
                val result = useCase.execute(GetProductsParam())
            ) {
                is Result.Success -> handleDataSucceed(result.value)
                is Result.Failure -> handleDataFailure(result.error)
            }
            setPageLoading(false)
        }
    }

    private fun handleDataFailure(error: String) {
        _failureEventLiveData.value = error
    }

    private fun handleDataSucceed(items: List<ProductDomainModel>) {
        val newItems = mapToPageItem(items)
        latestItems.clear()
        latestItems.addAll(newItems)
        _updateListLiveData.value = latestItems
        handleEmptyView()
    }

    private fun handleEmptyView() {
        val currentValues = _updateListLiveData.value ?: return
        _emptyViewVisibilityLiveData.value = currentValues.isEmpty()
    }

    private fun mapToPageItem(result: List<ProductDomainModel>): List<ProductListItem> =
        result.map { it.toListItem() }

    private fun setPageLoading(isLoading: Boolean) {
        _progressVisibilityLiveData.value = isLoading
    }

    fun onProductListRetryClick() {
        getProductList()
    }

    fun onUserChangedSearchString(query: String) {
        if (latestItems.isEmpty()) {
            return
        }
        val filteredList = if (query.isEmpty()) {
            latestItems
        } else {
            latestItems.filter {
                searchQuery(it, query)
            }
        }
        _updateListLiveData.value = filteredList
        handleEmptyView()
    }

    private fun searchQuery(
        it: ProductListItem,
        query: String
    ) = it.name.contains(query) || it.description.contains(query)

    fun onProductClicked(item: ProductListItem, adapterPosition: Int) {
        _navigateToDetailsLiveData.value = item to adapterPosition
    }
}