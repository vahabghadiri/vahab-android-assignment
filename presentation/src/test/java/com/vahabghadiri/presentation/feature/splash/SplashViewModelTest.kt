package com.vahabghadiri.presentation.feature.splash

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.nhaarman.mockitokotlin2.any
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.times
import com.nhaarman.mockitokotlin2.verify
import com.vahabghadiri.core.utils.GlobalDispatcher
import com.vahabghadiri.core.utils.testCase
import com.vahabghadiri.domain.feature.review.usecase.SubmitPendingReviewUseCase
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.runBlocking
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
internal class SplashViewModelTest {

    @get:Rule
    val instantTaskExecutorRule = InstantTaskExecutorRule()

    private val submitPendingReviewUseCase: SubmitPendingReviewUseCase = mock()
    private lateinit var globalDispatcher: GlobalDispatcher
    private lateinit var viewModel: SplashViewModel

    @Before
    fun setup() {
        globalDispatcher = GlobalDispatcher(
            main = Dispatchers.Unconfined,
            io = Dispatchers.Unconfined,
            default = Dispatchers.Unconfined
        )
        viewModel = SplashViewModel(
            submitPendingReviewUseCase,
            globalDispatcher
        )
    }

    @Test
    fun `should send unsent pending reviews on splash start`() = testCase {
        whenever(viewModel) {
            runBlocking {
                onSplashViewCreated()
                delay(2000)
            }
        }
        then {
            runBlocking {
                verify(submitPendingReviewUseCase, times(1)).execute(any())
            }
        }
    }
}