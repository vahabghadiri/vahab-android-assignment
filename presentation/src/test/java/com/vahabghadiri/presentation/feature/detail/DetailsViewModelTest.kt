package com.vahabghadiri.presentation.feature.detail

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.Observer
import com.nhaarman.mockitokotlin2.any
import com.nhaarman.mockitokotlin2.argumentCaptor
import com.nhaarman.mockitokotlin2.doReturn
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.stub
import com.nhaarman.mockitokotlin2.times
import com.nhaarman.mockitokotlin2.verify
import com.vahabghadiri.core.utils.GlobalDispatcher
import com.vahabghadiri.core.utils.testCase
import com.vahabghadiri.domain.feature.details.model.ProductReviewDomainModel
import com.vahabghadiri.domain.feature.details.model.ProductReviewsDomainModel
import com.vahabghadiri.domain.feature.details.usecase.GetProductReviewsUseCase
import com.vahabghadiri.domain.feature.review.usecase.GetUserReviewUseCase
import com.vahabghadiri.domain.feature.review.usecase.SubmitReviewUseCase
import com.vahabghadiri.domain.model.Result
import com.vahabghadiri.presentation.feature.detail.model.ProductInfoModel
import com.vahabghadiri.presentation.feature.detail.model.ProductReviewListItem
import com.vahabghadiri.presentation.feature.products.model.ProductListItem
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.runBlocking
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
internal class DetailsViewModelTest {

    @get:Rule
    val instantTaskExecutorRule = InstantTaskExecutorRule()

    private val detailsUseCase: GetProductReviewsUseCase = mock()
    private val submitReviewUseCase: SubmitReviewUseCase = mock()
    private val getUserReviewUseCase: GetUserReviewUseCase = mock()
    private lateinit var globalDispatcher: GlobalDispatcher
    private lateinit var viewModel: DetailsViewModel

    @Before
    fun setup() {
        globalDispatcher = GlobalDispatcher(
            main = Dispatchers.Unconfined,
            io = Dispatchers.Unconfined,
            default = Dispatchers.Unconfined
        )
        viewModel = DetailsViewModel(
            detailsUseCase,
            submitReviewUseCase,
            getUserReviewUseCase,
            globalDispatcher
        )
    }

    @Test
    fun `should update ui with text info on page creation`() = testCase {
        val observer = mock<Observer<ProductInfoModel>>()
        val argumentCaptor = argumentCaptor<ProductInfoModel>()
        val fakeItem = ProductListItem("a", "b", "c", "0", "")
        given {
            viewModel.updateInfoModelLiveData.observeForever(observer)
        }
        whenever(viewModel) {
            onViewCreated(fakeItem)
        }
        then {
            verify(observer, times(1)).onChanged(argumentCaptor.capture())
        }
    }

    @Test
    fun `should get product reviews on page creation`() = testCase {
        val observer = mock<Observer<List<ProductReviewListItem>>>()
        val argumentCaptor = argumentCaptor<List<ProductReviewListItem>>()
        val fakeItem = ProductListItem("a", "b", "c", "0", "")
        given {
            viewModel.updateReviewsLiveData.observeForever(observer)
            detailsUseCase.stub {
                onBlocking { execute(any()) } doReturn Result.Success(
                    ProductReviewsDomainModel(
                        fakeItem.id, emptyList()
                    )
                )
            }
        }
        whenever(viewModel) {
            onViewCreated(fakeItem)
        }
        then {
            verify(observer, times(1)).onChanged(argumentCaptor.capture())
        }
    }
}